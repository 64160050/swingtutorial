package com.tanisorn.swingtutorial;

import javax.swing.*;
class MyApp {
    JFrame frame;
    JButton button;
    public MyApp() {
        frame = new JFrame();
        frame.setTitle("First JFrame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        frame.setLayout(null);
        frame.add(button);
        frame.setSize(400, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
public class Simple {
    public static void main(String[] args) {
      MyApp app = new MyApp();
    }
}
